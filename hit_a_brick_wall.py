#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys, pygame, time, random

size = width, height = (800, 700)
bg_color = (80, 60, 0)
brick_wall, brick_rows, ticks = [], 8, 80
ball_init_speed = paddle_init_speed = item_init_speed = None


def game_init(ball_speed=(0, 8), paddle_speed=25, item_speed=(0,8)):
    """Initial routine for initial variables, a window, sounds, images, etc."""
    global ball_init_speed, paddle_init_speed, item_init_speed

    ball_init_speed, paddle_init_speed ,item_init_speed= ball_speed, paddle_speed, item_speed

    pygame.mixer.pre_init(44100, -16, 1, 512)
    pygame.init()
    pygame.key.set_repeat(1, 30)
    pygame.mouse.set_visible(0)

    screen = pygame.display.set_mode(size)
    pygame.display.set_caption("Hit A Brick Wall")

    hit_paddle_sound = pygame.mixer.Sound('hitDown.wav')
    hit_brick_sound = pygame.mixer.Sound('hitTop.wav')
    hit_top_sound = pygame.mixer.Sound('hitUp.wav')
    end_sound = pygame.mixer.Sound('theEnd.wav')
    bg_sound = pygame.mixer.Sound('bgsound.wav')
    vic_sound = pygame.mixer.Sound('victory.wav')
    over_sound = pygame.mixer.Sound('gameover.wav')

    paddle_surface = pygame.image.load("paddle.png").convert_alpha()
    ball_surface = pygame.image.load("ball.png").convert_alpha()
    brick_surface = pygame.image.load("brick.png").convert_alpha()
    lbrick_surface = pygame.image.load("lifebrick.png").convert_alpha()
    sbrick_surface = pygame.image.load("20brick.png").convert_alpha()
    hbrick_surface = pygame.image.load("hbrick.png").convert_alpha()
    bg_surface = pygame.image.load("bg.png").convert()

    paddle = paddle_surface.get_rect()
    ball = ball_surface.get_rect()
    brick = brick_surface.get_rect()
    lbrick = lbrick_surface.get_rect() 
    sbrick = sbrick_surface.get_rect()
    hbrick = hbrick_surface.get_rect()
    bg = bg_surface.get_rect()
    

    paddle.move_ip(width/2 - paddle.width/2, height - 25)
    ball.move_ip(width/2 - ball.width/2, height/2)
    return screen, hit_paddle_sound, hit_brick_sound, hit_top_sound, end_sound, bg_sound, vic_sound, over_sound, \
        paddle_surface, ball_surface, brick_surface, lbrick_surface, sbrick_surface, bg_surface, hbrick_surface, paddle, ball, brick, lbrick, sbrick, hbrick, bg

def main():
    """The main() function is called when the program start."""
    screen, hit_paddle_sound, hit_brick_sound, hit_top_sound, end_sound, bg_sound, vic_sound, over_sound, \
        paddle_surface, ball_surface, brick_surface, lbrick_surface, sbrick_surface,  bg_surface, hbrick_surface, \
        paddle, ball, brick, lbrick, sbrick, hbrick, bg = game_init()

    score = 0
    move_ball_x, move_ball_y = ball_init_speed
    move_paddle = paddle_init_speed
    clock = pygame.time.Clock()
    life=5
    move_item_x,move_item_y = item_init_speed
    level = 1

    
    def setup_bricks(y=80, w=width, rows=brick_rows):
        bw, bh = brick.size
        brick_wall.clear()
        for i in range(104):
            if i == 58:
                brick_wall.append(lbrick.move((i%13)*bw+40, y+i//13*bh))
            if i in [44,45,46,57,59,70,71,72]:
                brick_wall.append(sbrick.move((i%13)*bw+40, y+i//13*bh))
            if i not in [44,45,46,57,58,59,70,71,72]:
                brick_wall.append(brick.move((i%13)*bw+40, y+i//13*bh))

    def setup_bricks2(y=80, w=width, rows=brick_rows):
        bw, bh = brick.size
        brick_wall.clear()
        for i in range(104):
            if i == 58:
                brick_wall.append(lbrick.move((i%13)*bw+40, y+i//13*bh))
            if i in [44,45,46,57,59,70,71,72]:
                brick_wall.append(sbrick.move((i%13)*bw+40, y+i//13*bh))
            if (i not in [44,45,46,57,58,59,70,71,72]) and (i%2 != 0):
                brick_wall.append(brick.move((i%13)*bw+40, y+i//13*bh))
            if (i not in [44,45,46,57,58,59,70,71,72]) and (i%2 == 0):
                brick_wall.append(brick.move((i%13)*bw+40, y+i//13*bh))
                brick_wall.append(hbrick.move((i%13)*bw+40, y+i//13*bh))
                
    def setup_bricks3(y=80, w=width, rows=brick_rows):
        bw, bh = brick.size
        brick_wall.clear()
        for i in range(104):
            if i == 58:
                brick_wall.append(lbrick.move((i%13)*bw+40, y+i//13*bh))
            if i in [44,45,46,57,59,70,71,72]:
                brick_wall.append(sbrick.move((i%13)*bw+40, y+i//13*bh))
            if (i not in [44,45,46,57,58,59,70,71,72]):
                brick_wall.append(brick.move((i%13)*bw+40, y+i//13*bh))
                brick_wall.append(hbrick.move((i%13)*bw+40, y+i//13*bh))    


    def quit():
        """Exit the program."""
        pygame.quit()
        sys.exit()

    def event_handler(event):
        """Keyboard event handling routine."""
        nonlocal paddle
        if event.type == pygame.QUIT:  # e.g. Close a game window
            quit()
        elif event.type == pygame.KEYDOWN:
            if event.key in [pygame.K_ESCAPE, ord('q'), ord('Q')]:
                quit()
            elif event.key == pygame.K_LEFT:
                paddle.move_ip(-move_paddle, 0)
                if paddle.left < 0:
                    paddle.left = 0
            if event.key == pygame.K_RIGHT:
                paddle.move_ip(move_paddle, 0)
                if paddle.right > width:
                    paddle.right = width
                    
                    
    def blink_text(text=""):
        """Display a blinking text in the middle of the window, while waiting for
        users to exit the program.
        """
        buffer = pygame.Surface(pygame.display.get_surface().get_size())
        buffer.blit(pygame.display.get_surface(), (0,0))

        font = pygame.font.Font(None,70)
        text_surface = font.render(text, True, (0,255,0), bg_color)
        surface = pygame.Surface(font.size(text))
        surface.blit(text_surface, (0, 0))
        surface.set_colorkey(bg_color)
        text_rect = text_surface.get_rect()
        text_rect.move_ip(width/2 - text_rect.centerx, 0.4*height)
        dir, alpha = -5, 255
        while True:
            clock.tick(ticks)
            for event in pygame.event.get():
                event_handler(event)
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_y:
                        main()
                    if event.key == pygame.K_n:
                        quit()
            pygame.display.get_surface().blit(buffer,(0,0))
            surface.set_alpha(alpha)
            screen.blit(surface, text_rect)
            pygame.display.flip()
            alpha += dir
            if alpha in [0, 255]:
                dir = -dir

    setup_bricks()  # Create a brick wall
    bg_sound.play(10)
    print(brick_wall)
    while True:
        clock.tick(ticks)
        for event in pygame.event.get():
            event_handler(event)

        if paddle.bottom-move_ball_y >= ball.bottom >= paddle.top and \
           paddle.right >= ball.centerx >= paddle.left:  # Ball hits the paddle
            if ball.centerx < paddle.right  and ball.centerx > paddle.centerx:
                move_ball_x = random.randint(2,7)
            if ball.centerx > paddle.left and ball.centerx < paddle.centerx:
                move_ball_x = -(random.randint(2,7))
            if ball.centerx == paddle.right:
                move_ball_x = 8
            if ball.centerx == paddle.left :
                move_ball_x = -8
            if ball.centerx == paddle.centerx:
                move_ball_x = random.randint(-1,1)
            move_ball_y = -move_ball_y  # Bounce the ball back
            hit_paddle_sound.play(0)
            # !!! Add or Modify code:
            # Adjust the bouncing direction of the ball to be more desirable.
            #
            #
            #

        ball.move_ip(move_ball_x, move_ball_y)    # Move ball to new position
        if ball.top > height :
            end_sound.play(0)
            life -= 1
            ball.top = 350
            ball.right = 424
            move_ball_x = 0
            move_ball_y = 0
            if life == -1 :
                bg_sound.stop()
                over_sound.play(0)
                blink_text("Continue? (y/n)")
        
        if len(brick_wall) ==0 :
            level += 1
            if level == 2:
                setup_bricks2()
                ball.top = 350
                ball.right = 424
                move_ball_x = 0
                move_ball_y = 0 
            if level == 3:
                setup_bricks3()
                ball.top = 350
                ball.right = 424
                move_ball_x = 0
                move_ball_y = 0  
            if level == 4:
                bg_sound.stop()
                vic_sound.play(0)
                blink_text("You win!!! (Try again(y/n)")

        if ball.top < 0:    # The ball hits the upper side of the window
            move_ball_y = -move_ball_y
            hit_top_sound.play(0)
        
        if ball.left<0:
            move_ball_x = -move_ball_x
            hit_top_sound.play(0)
        
        if ball.right>width:
            move_ball_x = -move_ball_x
            hit_top_sound.play(0)
        
        if move_ball_x==0 and move_ball_y==0 :
            if paddle.centerx == 400 or paddle.centerx == 412 or paddle.centerx == 413:
                move_ball_y = 8
                

            

        
        index = ball.collidelist(brick_wall)  # Find a brick that the ball hits
        if index >= 0:
            
            if ball.top < brick_wall[index].bottom or ball.bottom > brick_wall[index].top:
                if ball.centerx > brick_wall[index].left and ball.centerx < brick_wall[index].right:
                    move_ball_y = -move_ball_y
                else:
                    move_ball_x = -move_ball_x
            

            if list(brick_wall[index])[2:4]==[54,18] :
                life += 1

            
            if list(brick_wall[index])[2:4]==[55,17]:
                score += 20
            else:
                score += 10
                
            if index+1 < len(brick_wall):    
                if list(brick_wall[index+1])[2:4]==[56,18] :
                    brick_wall.pop(index+1)
                else:
                    brick_wall.pop(index)
            else:
                brick_wall.pop(index)
            
       
            
            hit_brick_sound.play(0)
                            
            # !!! Modify code:
            # Display the score on the screen.
            #  
            print(score)

        screen.fill(bg_color)                 # Clear the screen 
        screen.blit(bg_surface, bg)
        
        for b in brick_wall:
            if list(b)[2:4] == [54,18]:
                screen.blit(lbrick_surface, b)
            if list(b)[2:4] == [55,17]:
                screen.blit(sbrick_surface, b)
            if list(b)[2:4] == [55,18]:
                screen.blit(brick_surface, b)
            if list(b)[2:4] == [56,18]:
                screen.blit(hbrick_surface, b)                
        

        screen.blit(ball_surface, ball)       # Display the ball
        screen.blit(paddle_surface, paddle)   # Display the paddle
            
        
        fontscore = pygame.font.Font(None,36)
        score_surface = fontscore.render("Score : "+str(score), True, (255,0,0), bg_color)
        scoresurface = pygame.Surface(fontscore.size("Score : "+str(score)))
        scoresurface.blit(score_surface, (0, 0))
        scoresurface.set_colorkey(bg_color)
        score_rect = score_surface.get_rect(center=(70,20))
        screen.blit(scoresurface,score_rect )
        
        fontlife = pygame.font.Font(None,36)
        life_surface = fontlife.render("Life left : "+str(life), True, (255,0,0), bg_color)
        lifesurface = pygame.Surface(fontlife.size("Life left : "+str(life)))
        lifesurface.blit(life_surface, (0,0))
        lifesurface.set_colorkey(bg_color)
        life_rect = life_surface.get_rect(center=(235,20))
        screen.blit(lifesurface,life_rect)
        
        fontlevel = pygame.font.Font(None,36)
        level_surface = fontlevel.render("Level : "+str(level), True, (255,0,0), bg_color)
        levelsurface = pygame.Surface(fontlevel.size("Level : "+str(level)))
        levelsurface.blit(level_surface, (0,0))
        levelsurface.set_colorkey(bg_color)
        level_rect = level_surface.get_rect(center=(400,20))
        screen.blit(levelsurface,level_rect)
        

        pygame.display.flip()

if __name__ == '__main__':
    main()
